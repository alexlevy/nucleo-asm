.syntax unified
.cpu    cortex-m4
.thumb

.global setup_ld3

setup_ld3:
  push  {r4-r6, lr}

  ldr   r4, =GPIOB_MODER
  ldr   r5, [r4]
  mov   r6, #0x1
  bfi   r5, r6, #6, #2   // PB3 = output
  str   r5, [r4]

  ldr   r5, [r4, #0xc]
  orr   r5, #0x40        // PUPD3 = pull-up
  str   r5, [r4, #0xc]

  ldr   r5, [r4, #0x18]  // GPIOB_BSRR
  orr   r5, #0x8         // BS3 = 1 => LD3 on
  str   r5, [r4, #0x18]

  pop   {r4-r6, pc}
