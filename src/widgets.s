.syntax unified
.cpu    cortex-m4
.thumb

.global display_stack

display_stack:
  push  {r0-r3, r4-r8, r12, lr}

  mov   r12, sp

  ldr   r0, =I2C1_CR1
  mov   r3, #0
  bl    set_page

  @ Set position of the cursor
  mov   r1, #3
  mov   r2, #0
  ldr   r4, =#0x100000   // command mode / lower nibble = 00 / higher nibble = 00
  push  {r4}
  bl    i2c_send_bytes

  @ Display the stack content
  sub   sp, #8           // allocate 2 words to hold the stack word to display
  mov   r1, #5           // 5 bytes to display a word in stack
  mov   r5, #8           // we start above the space we allocated
  ldr   r7, =_estack     // stop condition = end of stack
0:
  ldr   r4, [sp, r5]     // load the word in the stack
  ubfx  r6, r4, #24, #8  // extract the last byte
  lsl   r4, #8           // shift left to make room for oled command mode byte
  orr   r4, #0x40        // add command mode byte
  str   r4, [sp]         // store the word to display in the space we allocated
  strb  r6, [sp, #4]     // store the 4th byte
  bl    i2c_send_bytes   // display
  add   r5, #4           // proceed to next word in stack
  add   r8, sp, r5
  rsbs  r8, r7
  bne   0b               // stop if we are at the top of the stack

  mov   sp, r12

  pop   {r0-r3, r4-r8, r12, pc}
