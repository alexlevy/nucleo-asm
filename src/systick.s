.syntax unified
.cpu    cortex-m4
.thumb

.global setup_systick

setup_systick:
  push  {r4-r6, lr}

  ldr   r4, =STK_CTRL

  ldr   r5, [r4, #0x4]  // STK_LOAD
  ldr   r6, =#10000000  // 10,000,000 ticks at 80MHz/8 = 1 second
  str   r6, [r4, #0x4]

  ldr   r5, [r4, #0x8]  // STK_VAL
  bfc   r5, #0, #24
  str   r5, [r4, #0x8]

  ldr   r5, [r4]        // STK_CTRL
  orr   r5, #0x3        // TICKINT = 1 / ENABLE = 1
  str   r5, [r4]

  pop   {r4-r6, pc}
