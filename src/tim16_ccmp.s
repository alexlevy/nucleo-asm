.syntax unified
.cpu    cortex-m4
.thumb

.global setup_tim16
.global start_tim16

setup_tim16:
  push  {r4-r6, lr}

  ldr   r4, =TIM16_CR1
  ldrh  r5, [r4]
  orr   r5, #0x6         // UDIS = 1 / URS = 1
  strh  r5, [r4]

  ldr   r5, [r4, #0x18]  // TIM16_CCMR1
  orr   r5, #0x2d        // IC1F: fSAMPLING=fCK_INT, N=4 / IC1PSC = 8 / CC1S = TI1
  str   r5, [r4, #0x18]

  ldr   r5, [r4, #0x20]  // TIM16_CCER
  orr   r5, #0x1         // CC1E = 1
  str   r5, [r4, #0x20]

  ldr   r5, [r4, #0x50]  // TIM16_OR1
  orr   r5, #0x2         // TI1_RMP = LSE
  str   r5, [r4, #0x50]

  ldr   r5, [r4, #0x0c]  // TIM16_DIER
  orr   r5, #0x2         // CC1IE = 1 / UIE = 0
  str   r5, [r4, #0x0c]

  pop   {r4-r6, pc}

start_tim16:
  push  {r4-r6, lr}

  ldr   r4, =TIM16_CR1
  ldr   r5, [r4]
  mov   r6, #0x5         // URS = 1 / UDIS = 0 / CEN = 1
  bfi   r5, r6, #0, #4
  str   r5, [r4]

  pop   {r4-r6, pc}
