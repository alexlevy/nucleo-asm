OBJS = main.o vectors.o reg.o clocks.o flash.o interrupts.o rcc.o i2c.o tim.o tim16_ccmp.o ld3.o systick.o ssd1306.o bitband.o font.o itoa.o widgets.o

all: main

%.o: src\%.s
	arm-none-eabi-as -g -mcpu=cortex-m4 -mthumb $< -o $@

%.hex: %.elf
	arm-none-eabi-objcopy -O ihex $< $@

main.elf: $(OBJS)
	arm-none-eabi-ld $^ -T link.x -o $@

.PHONY: clean
clean:
	del *.hex
	del *.o
	del *.elf

.PHONY: main
main: main.hex
	openocd -f interface/stlink.cfg -f target/stm32l4x.cfg -c "init" -c "program main.hex verify reset exit"

%.d: %.elf
	openocd -f interface/stlink.cfg -f target/stm32l4x.cfg

%.p: %.elf
	arm-none-eabi-objdump -s -d $<
