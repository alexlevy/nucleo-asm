# STM32L432KC (Nucleo board) in assembler

## Setup

1. Download and install [GNU ARM embedded toolchain](https://developer.arm.com/downloads/-/gnu-rm)
2. Downlaod and install [MSYS2](https://www.msys2.org/)
3. In UCRT64 MSYS2 console, install [OpenOCD](https://openocd.org/) with `pacman -S ucrt64/mingw-w64-ucrt-x86_64-openocd`

## Build and debug

* Run `make` to compile `minimal.s` and flash it to the board
* Run `make minimal.elf debug` to run OpenOCD
* In another command line run `arm-none-eabi-gdb -x script.gdb -f minimal.elf`

## Tips to debug
* `x/i $pc` shows the current instruction in `PC` register
* `p/x $sp` shows the content of `SP` register
* `x/x $r3` shows the content from the address in `R3` register
* `x/x 0x40021000` shows the hex contents of `RCC_CR` register
* `x/4tb $r1` shows the contents from the address in `R1` register and prints it as 4 bytes
* `p/d (int)CPU_FREQ` shows the value of static symbol `CPU_FREQ` (in `.data` section)
* `p/a (int*)&CPU_FREQ` shows the address of the 4-bytes word `CPU_FREQ`
* `p/a (int*)&_edata` shows the address of the symbol `_edata` defined in the linker script
* `i br` shows breakpoints
* `del br 3` deletes breakpoint 3
* `watch (int*)&CPU_FREQ` defines a RW data watchpoint on variable `CPU_FREQ`
* `i wat` shows the watchpoints
* `del 3` deletes watchpoint 3
* `target extended-remote :3333` followed by `r` to reconnect GDB to OpenOCD and restart firmware after reflashing with `make`
* `make minimal.dmp` disassembles `minimal.elf`

## Coding rules
* Use `r0-r3` for function arguments
* Use `r0` for function return value
* Use `r4-r11` for temp variables
* Use `r12` for stack frame pointer in both interrupt routines and functions (see below)
* Use the stack for arrays and data structures

## Stack frame pointer

### Example code

```nasm
main:
    push {r0, r1, r12}
    mov r12, sp             ; set R12 at the bottom of the stack (= stack frame pointer)
    sub sp, #12
    ldr r0, =0xdeadbeef
    ldr r1, =0xf00df00d
    stmia sp, {r0, r1}
    bl myfunc
    mov sp, r12             ; restore stack pointer to where it was (i.e. delete the stack frame)
    pop {r0, r1, r12}

myfunc:
    push {r1, r12, lr}
    mov r12 sp
    sub sp, #8
    ldr r1, =0x1337beef
    str r1, [sp]
    mov sp, r12
    pop {r1, r12, pc}
```

### Representation of the stack

```
+-- (main) mov r12 sp // (main) mov sp r12 // [sp = r12 = 0x20010000]
|
| 0xf00df00d
| 0xdeadbeef
+-- (main) sub sp, #12  // (myfunc) mov r12 sp // (myfunc) mov sp r12 // [sp = r12 = 0x2001FFEO]
|
| 0x1337beef
+-- (myfunc) sub sp, #4 [sp = 0x2001FFDC, r12 = 0x2001FFE0]
```

## Simple program

`src/main.s` sets up the microcontroller in a relatively simple way :

1. Use `MSI` clock with `PLL` as the system clock
2. Use `LSE` crystal oscillator (precise 32.768 KHz) to calibrate `MSI`
3. Use `TIM16` in capture and compare mode with channel 1 input being `LSE`
4. On every 8th rising edge of `LSE` clock, store (`TIM16` CCR1 counter * 32768 / 8) into `CPU_FREQ` in RAM

Use `p/d (int)CPU_FREQ` in GDB to see the actual CPU frequency.

*(You can change MSI frequency by changing the `PLLN` constant in the `.equ` statement in `src/clocks.s`)*
