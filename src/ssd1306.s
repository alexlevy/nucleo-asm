.syntax unified
.cpu    cortex-m4
.thumb

.global clear_screen
.global print_char
.global print_string
.global setup_oled
.global set_page

setup_oled:
  push  {r0-r2, r4-r6, lr}

  ldr   r4, =GPIOB_MODER

  ldr   r5, [r4]
  mov   r6, #0xa
  bfi   r5, r6, #12, #4   // PB6 = AF / PB7 = AF
  str   r5, [r4]

  ldr   r5, [r4, #0x4]    // GPIOB_OTYPER
  orr   r5, #0xc0         // PB6 = open-drain / PB7 = open-drain
  str   r5, [r4, #0x4]

  ldr   r5, [r4, #0x8]    // GPIOB_OSPEEDR
  mov   r6, #0xc
  bfi   r5, r6, #12, #4   // PB6 = high-speed / PB7 = high-speed
  str   r5, [r4, #0x8]

  ldr   r5, [r4, #0xc]    // GPIOB_PUPDR
  mov   r6, #0x5
  bfi   r5, r6, #12, #4   // PB6 = pull-up / PB7 = pull-up
  str   r5, [r4, #0xc]

  ldr   r5, [r4, #0x20]   // GPIOB_AFRL
  mov   r6, #0x44         // PB6 = AF4 / PB7 = AF4
  bfi   r5, r6, #24, #8
  str   r5, [r4, #0x20]

  ldr   r4, =I2C1_CR1

  ldr   r5, [r4, #0x10]   // I2C1_TIMINGR
  ldr   r6, =0x90a42327   // PRESC = 0x9 / SCLDEL = 0xa / SDADEL = 0x4 / SCLH = 0x23 / SCLL = 0x27
  orr   r5, r6
  str   r5, [r4, #0x10]

  ldr   r5, [r4]
  mov   r6, 0x501         // DNF = 5 / PE = 1
  orr   r5, r6
  str   r5, [r4]

  ldr   r5, [r4, #0x4]    // I2C1_CR2
  ldr   r6, =0x2000078    // AUTOEND = 1 / SADD = 0x3C << 1 = 0x78
  orr   r5, r6
  str   r5, [r4, #0x4]

  ldr   r0, =I2C1_CR1
  mov   r1, #4            // 4 bytes to send
  mov   r2, #0            // offset in byte array
  ldr   r6, =#0xaf148d00  // command mode -> charge pump setting -> charge pump on -> display on
  push  {r6}
  bl    i2c_send_bytes
  add   sp, #4            // restore stack pointer

  pop   {r0-r2, r4-r6, pc}

@ r0 = I2C base register address
@ r3 = page number (0~7)
set_page:
  push  {r1-r2, r4, r12, lr}

  mov   r12, sp           // save stack pointer

  mov   r1, #0x2
  mov   r2, #0
  add   r4, r3, #0xb0
  lsl   r4, #8            // shift by 8 to add 00 at front (command mode)
  push  {r4}
  bl    i2c_send_bytes

  mov   sp, r12           // restore stack pointer

  pop   {r1-r2, r4, r12, pc}

clear_screen:
  push  {r0-r5, r12, lr}

  mov   r3, #0x0
  bl    set_page

  mov   r12, sp

  mov   r1, #0x81         // 128 bytes of 0s to send + 1 byte for write command
  mov   r2, #0
  mov   r4, #0x0
  mov   r5, #0x20         // 32 words of 0s to clear a full page (4 bytes is half a column)

0:
  push  {r4}              // load a word of 0s
  subs  r5, #1
  bne   0b
  push  {r4}              // we need an extra byte of 0s because we send the command
  ldr   r4, =#0x40        // write data command
  push  {r4}

0:
  bl    i2c_send_bytes    // clear 8 columns

  add   r3, #1            // go to next page
  bl    set_page

  cmp   r3, #0x8          // is it last page ?
  bne   0b                // no => continue to next page

  mov   sp, r12           // reset stack pointer

  pop   {r0-r5, r12, pc}

@ r3: char to print
print_char:
  push  {r0-r2, r4-r6, r12, lr}

  mov   r12, sp           // save stack pointer

  sub   r4, r3, #0x20     // character line in FONT
  mov   r5, #5
  mul   r4, r5            // index of first byte of character in FONT
  ldr   r5, =FONT
  add   r5, r4

  add   r5, #3            // start with last half word as we push to stack
  ldrh  r6, [r5], #-4     // load last half word and go back to first byte of character
  push  {r6}
  ldr   r6, [r5]          // load first 4 bytes of character
  bfc   r6, #0, #8        // remove the first byte (it is already in the last half word)
  add   r6, #0x40         // add write data command
  push  {r6}

  ldr   r0, =I2C1_CR1
  mov   r1, #6
  mov   r2, #0
  bl    i2c_send_bytes

  mov   sp, r12           // restore stack pointer

  pop   {r0-r2, r4-r6, r12, pc}

@ r0: length of string
@ r1: pointer for input string or 0 = use stack
print_string:
  push  {r0-r2, r4-r8, r12, lr}

  mov   r12, sp

  cmp   r1, #0
  ite   eq
  addeq r8, r12, #40      // pointer where the input string is stored
  movne r8, r1

  sub   sp, #8            // allocate enough space for all chars to print

  mov   r6, #5            // offset in FONT
  mov   r7, r0            // decrementing counter for chars to print

  ldr   r0, =I2C1_CR1
  mov   r1, #6            // a char is 6 bytes to display (0x40 + 5 bytes for the pixel columns)
  mov   r2, #0

0:
  ldrb  r4, [r8], #1      // load character to print from input string
  sub   r4, #0x20         // character line in FONT
  mov   r6, #5
  mul   r4, r6            // index of first byte of character in FONT
  ldr   r5, =FONT
  add   r5, r4

  ldr   r6, [r5], #3      // load first word and increment pointer by 3
  lsl   r6, #8            // make some room to replace first byte with command mode
  orr   r6, #0x40         // first byte is command mode
  str   r6, [sp]          // store first word in stack
  ldrh  r6, [r5]          // load last half word of character
  strh  r6, [sp, #4]      // store it in stack

  bl    i2c_send_bytes

  subs  r7, #1
  bne   0b

  mov   sp, r12

  pop   {r0-r2, r4-r8, r12, pc}
