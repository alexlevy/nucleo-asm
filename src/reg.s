.syntax unified
.cpu    cortex-m4
.thumb


.equ    FLASH_ACR   , 0x40022000
.equ    GPIOA_MODER , 0x48000000
.equ    GPIOB_MODER , 0x48000400
.equ    I2C1_CR1    , 0x40005400
.equ    NVIC_ISER0  , 0xe000e100
.equ    PERIPH_ALIAS, 0x42000000
.equ    PWR_CR1     , 0x40007000
.equ    RCC_CR      , 0x40021000
.equ    STK_CTRL    , 0xe000e010
.equ    TIM7_CR1    , 0x40001400
.equ    TIM16_CR1   , 0x40014400

.global FLASH_ACR
.global GPIOA_MODER
.global GPIOB_MODER
.global I2C1_CR1
.global NVIC_ISER0
.global PERIPH_ALIAS
.global PWR_CR1
.global RCC_CR
.global STK_CTRL
.global TIM7_CR1
.global TIM16_CR1
