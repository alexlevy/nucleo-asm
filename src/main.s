.syntax  unified
.cpu     cortex-m4
.thumb

.equ     LSE_FREQ, 32768

.global  i2c1_handler
.global  reset_handler
.global  systick_handler
.global  tim16_handler

.section .data

CPU_ICON:
  .byte  0x40, 0x6b, 0x1c, 0x7f, 0x1c, 0x6b
CPU_FREQ:
  .word  0x0

.section .text

.thumb_func
reset_handler:
  cpsid  i                     // disable interrupts
  ldr    r4, =_sidata          // LMA of data section (in FLASH)
  ldr    r5, =_sdata           // start of data section (in RAM)
  ldr    r6, =_edata           // end of data section (in RAM)
  sub    r7, r6, r5            // length of data section
  cbz    r7, init_peripherals  // branch to init_peripherals if nothing to copy

copy_data_to_ram:
  ldrb   r6, [r4]              // load byte from data section
  strb   r6, [r5]              // store byte in RAM
  add    r4, #1
  add    r5, #1
  subs   r7, #1
  bne    copy_data_to_ram

init_peripherals:
  bl     init_flash
  bl     init_clocks
  bl     init_interrupts
  bl     init_gpio
  bl     init_i2c1
  bl     init_tim16
  bl     init_tim7
  cpsie  i                     // enable interrupts

main:
  @ Setup peripherals
  bl     setup_tim16
  bl     setup_systick
  bl     setup_ld3
  bl     setup_oled
  bl     start_tim16           // start capture of CPU frequency

  mov    r12, sp               // save stack pointer

  ldr    r0, =I2C1_CR1

  @ Display GDDRAM content
  mov    r1, #2                // 2 bytes to send
  mov    r2, #0                // start at beginning of byte array
  ldr    r4, =#0xa400          // command mode -> display GDDRAM
  push   {r4}
  bl     i2c_send_bytes

  bl     clear_screen

  @ Go to page 7 of OLED
  mov    r3, #0x7
  bl     set_page

  @ Reset position of the cursor
  mov    r1, #3
  ldr    r4, =#0x100000        // command mode / lower nibble = 00 / higher nibble = 00
  push   {r4}
  bl     i2c_send_bytes

  @ Display CPU icon
  mov    r1, #6
  sub    sp, #8
  ldr    r4, =CPU_ICON
  ldr    r5, [r4]
  str    r5, [sp]
  ldrh   r5, [r4, #4]
  str    r5, [sp, #4]
  bl     i2c_send_bytes

  @ Move cursor position for Hz suffix
  mov    r1, #3
  ldr    r4, =#0x130200        // command mode / lower nibble = 04 / higher nibble = 03
  push   {r4}
  bl     i2c_send_bytes

  @ Display Hz suffix
  mov    r4, #' '
  strb   r4, [sp]
  mov    r4, #'H'
  strb   r4, [sp, #1]
  mov    r4, #'z'
  strb   r4, [sp, #2]
  mov    r0, #3
  mov    r1, #0
  bl     print_string

  mov    sp, r12               // restore stack pointer

  b      loop

loop:
  b      .

.thumb_func
tim16_handler:
  push   {r4-r9, lr}

  ldr    r8, =GPIOB_MODER
  add    r8, #0x18             // GPIOB_BSRR

  mov    r9, #0x80000          // BR3 = 1 => LD3 off
  str    r9, [r8]

  ldr    r4, =TIM16_CR1
  ldr    r5, [r4, #0x10]       // TIM16_SR
  mov    r6, #0x2
  bic    r5, r6                // CC1IF = 0
  str    r5, [r4, #0x10]

  ldr    r6, =CPU_FREQ
  ldr    r5, [r4, #0x34]       // TIM16_CCR1
  lsr    r5, r5, #3            // TIM16 IC1PSC = 8 so we need to divide counter value by 8
  ldr    r7, =LSE_FREQ
  mul    r5, r7
  str    r5, [r6]              // save CPU frequency

  ldr    r5, [r4, #0x14]       // TIM16_EGR
  orr    r5, #0x1              // UG = 1 => reset timer
  str    r5, [r4, #0x14]

  mov    r9, #0x8              // BS3 = 1 => LD3 on
  str    r9, [r8]

  pop    {r4-r9, pc}

.thumb_func
systick_handler:
  push   {r4, lr}

  mov    r12, sp               // save stack pointer

  ldr    r0, =I2C1_CR1

  mov    r3, #7
  bl     set_page

  @ Set position of the cursor
  mov    r1, #3
  mov    r2, #0
  ldr    r4, =#0x100c00        // command mode / lower nibble = 0c / higher nibble = 00
  push   {r4}
  bl     i2c_send_bytes

  ldr    r0, =CPU_FREQ
  sub    sp, #12
  mov    r1, sp                // write result to stack
  bl     itoa

  mov    r0, #8                // frequency is in the tens of millon so 8 digits
  bl     print_string

  mov    sp, r12               // restore stack pointer

  pop    {r4, pc}
