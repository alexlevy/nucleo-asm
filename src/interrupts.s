.syntax  unified
.cpu     cortex-m4
.thumb

.global  init_interrupts
.global  default_handler

.section .text

init_interrupts:
  push   {r4-r6, lr}

  ldr    r4, =NVIC_ISER0
  ldr    r5, [r4]
  ldr    r6, =#0x2000000  // Enable IRQ 25 (TIM16)
  orr    r5, r6
  str    r5, [r4]

  pop    {r4-r6, pc}

.thumb_func
default_handler:
  b      .
