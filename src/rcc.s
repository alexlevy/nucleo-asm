.syntax unified
.cpu    cortex-m4
.thumb

.global init_gpio
.global init_i2c1
.global init_tim16
.global init_tim7

init_gpio:
  push  {r4-r6, lr}

  ldr   r4, =RCC_CR
  ldr   r5, [r4, #0x4c]
  orr   r5, #0x2             // GPIOBEN = 1
  str   r5, [r4, #0x4c]

  pop   {r4-r6, pc}

init_i2c1:
  push  {r4-r6, lr}

  ldr   r4, =RCC_CR
  ldr   r5, [r4, #0x58]
  mov   r6, 0x1
  orr   r5, r5, r6, LSL #21  // I2C1EN = 1
  str   r5, [r4, #0x58]

  ldr   r4, =I2C1_CR1
  ldr   r5, [r4]
  mov   r6, #0x1
  bic   r5, r6               // PE = 0
  str   r5, [r4]

  pop   {r4-r6, pc}

init_tim16:
  push  {r4-r6, lr}

  ldr   r4, =RCC_CR
  ldr   r5, [r4, #0x60]      // RCC_APB2ENR
  mov   r6, #0x1             // TIM16EN = 1
  orr   r5, r5, r6, LSL #17
  str   r5, [r4, #0x60]

  pop   {r4-r6, pc}

init_tim7:
  push  {r4-r6, lr}

  ldr   r4, =RCC_CR
  ldr   r5, [r4, #0x58]
  orr   r5, #0x20            // TIM7EN = 1
  str   r5, [r4, #0x58]

  pop   {r4-r6, pc}
