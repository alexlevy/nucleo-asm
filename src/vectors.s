.section .vector_table

.word    reset_handler
.word    default_handler  // NMI
.word    default_handler  // Hard fault
.word    default_handler  // Memory management fault
.word    default_handler  // Bus fault
.word    default_handler  // Usage fault
.word    0                // Reserved
.word    0                // Reserved
.word    0                // Reserved
.word    0                // Reserved
.word    default_handler  // SVCall
.word    0                // Reserved for debug
.word    0                // Reserved
.word    default_handler  // PendSV
.word    systick_handler  // SysTick
.word    default_handler  // Window Watchdog
.word    default_handler  // PVD/PVM1/PVM2/PVM3/PVM4
.word    default_handler  // RTC Tamper or TimeStamp /CSS on LSE
.word    default_handler  // RTC Wakeup timer
.word    default_handler  // Flash global
.word    default_handler  // RCC global
.word    default_handler  // EXTI Line0
.word    default_handler  // EXTI Line1
.word    default_handler  // EXTI Line2
.word    default_handler  // EXTI Line3
.word    default_handler  // EXTI Line4
.word    default_handler  // DMA1 channel 1
.word    default_handler  // DMA1 channel 2
.word    default_handler  // DMA1 channel 3
.word    default_handler  // DMA1 channel 4
.word    default_handler  // DMA1 channel 5
.word    default_handler  // DMA1 channel 6
.word    default_handler  // DMA1 channel 7
.word    default_handler  // ADC1 and ADC2
.word    default_handler  // CAN1_TX
.word    default_handler  // CAN1_RX0
.word    default_handler  // CAN1_RX1
.word    default_handler  // CAN1_SCE
.word    default_handler  // EXTI Line[9:5]
.word    default_handler  // TIM1 Break/TIM15
.word    tim16_handler    // TIM1 Update/TIM16
.word    default_handler  // TIM1 trigger and commutation
.word    default_handler  // TIM1 capture compare
.word    default_handler  // TIM2 global
.word    default_handler  // TIM3 global
.word    0                // Reserved
.word    default_handler  // I2C1 event
