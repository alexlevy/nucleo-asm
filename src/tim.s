.syntax unified
.cpu    cortex-m4
.thumb

.global start_tim67

@ r0 = TIM6/7 base register address
@ r1 = prescaler value
@ r2 = auto-reload value
start_tim67:
  push  {r4, lr}

  ldr   r4, [r0]
  orr   r4, #0xc         // OPM = 1 / URS = 1
  str   r4, [r0]

  str   r1, [r0, #0x28]  // TIMx_PSC

  str   r2, [r0, #0x2c]  // TIMx_ARR

  ldr   r4, [r0, #0x14]  // TIMx_EGR
  orr   r4, #0x1         // UG = 1 (update ARR and PSC shadow registers)
  str   r4, [r0, #0x14]

  ldr   r4, [r0, #0x10]  // TIMx_SR
  bic   r4, #0x1         // clear UIF
  str   r4, [r0, #0x10]

  ldr   r4, [r0]
  orr   r4, #0x1         // CEN = 1
  str   r4, [r0]

0:
  ldr   r4, [r0, #0x10]  // TIMx_SR
  cmp   r4, #0x1         // UIF = 1 ?
  bne   0b               // UIF = 0 => loop

  pop   {r4, pc}
