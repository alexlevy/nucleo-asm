.syntax unified
.cpu    cortex-m4
.thumb

.global i2c_send_bytes

@ r0 = I2C base register address
@ r1 = byte array length
@ r2 = offset in byte array
i2c_send_bytes:
  push  {r0-r2, r4-r6, lr}

  mov   r6, sp
  add   r6, #28          // save stack pointer value before push to access byte array

  ldr   r4, [r0, #0x4]   // I2Cx_CR2
  bfi   r4, r1, #16, #8  // NBYTES = r1
  str   r4, [r0, #0x4]

  push  {r0-r2}
  uxth  r5, r0           // I2Cx register offset (e.g. 5400 for I2C1)
  ldr   r0, =PERIPH_ALIAS
  add   r1, r5, #0x5     // 2nd byte of e.g. 0x40005404 (I2C1_CR2) = 0x40005405
  mov   r2, #0x5         // 5th bit of e.g. 0x40005405 = bit 13 = START
  bl    bbw              // atomic START = 1
  pop   {r0-r2}

0:
  ldr   r4, [r0, #0x18]  // I2Cx_ISR
  ands  r4, #0x2         // TXIS
  beq   0b               // loop if TXIS = 0

  ldrb  r5, [r6, r2]     // load byte to send
  strb  r5, [r0, #0x28]  // I2Cx_TXDR => send byte on the wire
  add   r2, #1           // increment byte array index
  subs  r1, #1           // decrement counter of bytes to send
  bne   0b               // if more bytes to send then loop

  pop   {r0-r2, r4-r6, pc}
