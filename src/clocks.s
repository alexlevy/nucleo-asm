.syntax unified
.cpu    cortex-m4
.thumb

.equ    PLLN, 40

.global init_clocks

init_clocks:
  push  {r4-r6, lr}

  ldr   r4, =RCC_CR

  ldr   r5, [r4]             // RCC_CR
  mov   r6, #0x1
  bic   r5, r5, r6, LSL #24  // clear PLLON
  str   r5, [r4]

0:
  ldr   r5, [r4]             // RCC_CR
  ands  r5, #0x2000000       // PLLREADY ?
  bne   0b                   // PLLREADY = 1 => loop

  ldr   r5, [r4, #0x58]      // RCC_APB1ENR
  mov   r6, 0x1
  orr   r5, r5, r6, LSL #28  // PWREN = 1
  str   r5, [r4, #0x58]

  ldr   r4, =PWR_CR1
  ldr   r5, [r4]
  orr   r5, #0x100           // DBP = 1
  str   r5, [r4]

  ldr   r4, =RCC_CR
  ldr   r5, [r4, #0x90]      // RCC_BDCR
  orr   r5, #0x1             // LSEON = 1
  str   r5, [r4, #0x90]

0:
  ldr   r5, [r4, #0x90]      // RCC_BDCR
  ands  r5, #0x2             // LSERDY ?
  beq   0b                   // LSERDY = 0 => loop

  ldr   r5, [r4]             // RCC_CR
  orr   r5, #0x7d            // MSIRANGE = 8MHz / MSIRGSEL = 1 / MSIPLLEN = 1 / MSION = 1
  str   r5, [r4]

0:
  ldr   r5, [r4]             // RCC_CR
  ands  r5, #0x2             // MSIRDY ?
  beq   0b                   // MSIRDY = 0 => loop

  ldr   r5, [r4, #0xc]       // RCC_PLLCFGR
  ldr   r6, =PLLN            // CPU frequency should be (r6 * 2) MHz
  bfi   r5, r6, #8, #7       // PLLN
  orr   r5, #0x11            // PLLSRC = MSI / PLLM = 2 / PLLR = 2
  str   r5, [r4, #0xc]

  ldr   r5, [r4]             // RCC_CR
  mov   r6, #0x1
  orr   r5, r5, r6, LSL #24  // PLLON
  str   r5, [r4]

0:
  ldr   r5, [r4]             // RCC_CR
  ands  r5, r5, r6, LSL #25  // PLLREADY ?
  beq   0b                   // PLLREADY = 0 => loop

  ldr   r5, [r4, #0xc]       // RCC_PLLCFGR
  orr   r5, r5, r6, LSL #24  // PLLREN bit
  str   r5, [r4, #0xc]

  ldr   r5, [r4, #0x8]       // RCC_CFGR
  orr   r5, #0x3             // system clock as PLL
  str   r5, [r4, #0x8]

0:
  ldr   r5, [r4, #0x8]       // RCC_CFGR
  ubfx  r5, r5, #2, #2       // extract bits 3:2
  cmp   r5, #0x3             // SWS = PLL ?
  bne   0b                   // SWS != PLL => loop

  pop   {r4-r6, pc}
