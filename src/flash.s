.syntax unified
.cpu    cortex-m4
.thumb

.global init_flash

init_flash:
  push  {r4, r5, lr}

  ldr   r4, =FLASH_ACR
  ldr   r5, [r4]
  orr   r5, #0x4        // LATENCY = 4WS
  str   r5, [r4]

0:
  ldrb  r5, [r4]        // load lowest byte of FLASH_ACR
  ubfx  r5, r5, #0, #3  // extract first 3 bits
  cmp   r5, #0x4        // LATENCY = 4WS ?
  bne   0b              // no => loop

  pop   {r4, r5, pc}
