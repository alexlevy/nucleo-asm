.syntax  unified
.cpu     cortex-m4
.thumb

.global  itoa

@ r0 = address of number in memory
@ r1 = address of result
itoa:
  push   {r0-r1, r4-r9, lr}

  ldr    r4, =#1000000000  // we start with largest power of 10
  ldr    r0, [r0]          // load value of digit from memory
  mov    r6, #10           // divisor of r4 to extract a digit
  mov    r9, #0            // used for heading 0s detection (active low)

0:
  udiv   r5, r0, r4        // divide number by current power of 10
  cmp    r5, #0            // if 0 then power of 10 is larger than number or 0 in the middle
  ittet  eq
  orrseq r9, r5            // proceed to next digit only if we have seen 0s so far (heading 0s)
  udiveq r4, r6            // next power of 10
  movne  r9, #1            // update flag for heading 0s detection
  beq    0b                // if heading 0 then proceed to next digit
  add    r7, r5, #0x30     // convert digit to ascii
  strb   r7, [r1], #1      // store ascii representation of digit in result
  mul    r8, r5, r4        // power of 10 * result of division
  sub    r0, r8            // next number to process is the rest of the division
  udiv   r4, r6            // next power of 10
  cmp    r4, #0            // if r4 is 0 then we're done
  bne    0b

  pop    {r0-r1, r4-r9, pc}
