.syntax unified
.cpu    cortex-m4
.thumb

.global bbw

@ r0 = bit_band_base
@ r1 = byte_offset
@ r2 = bit_number
bbw:
  push  {r4, r5, lr}

  lsl   r4, r1, #5          // byte_offset * 32
  add   r4, r4, r2, LSL #2  // (byte_offset * 32) + (bit_number * 4)

  add   r4, r0              // bit_band_base + (byte_offset * 32) + (bit_number * 4)

  mov   r5, #1
  strb  r5, [r4]            // atomic read-modify-write

  pop   {r4, r5, pc}
